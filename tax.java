import java.util.*;
class Tax                                                                     //Tax as class name
{
  static double price,tax;
  public static void main(String s[])                                         //Main method
  {
    
    try{                                                                      //Try block _Exceptionla Handling
        char cont='y';
        while(cont=='y'){
          Scanner sc =new Scanner(System.in);
          System.out.println("Enter Item name:");
          String name= sc.nextLine();  
          System.out.println("Enter Item Price:");
          double price= sc.nextDouble();
          System.out.println("Enter Item Quantity:");
          int quantity= sc.nextInt();
          System.out.println("Enter Item Type:");
          System.out.println("Raw\tManufactured.\tImported.");
          //int type =sc.nextInt();
          String type=sc.next();
          type=type.toLowerCase();
          System.out.println("Item_name Item_price Item_quantity"); 
          System.out.println(name+"\t "+price+"\t\t "+quantity);

          switch(type)                                                        //Switch case beginning
          {
            case "raw":
                tax=price*0.125;
                System.out.println(tax);
                break;

            case "manufactured":
                tax=price*0.125;
                tax+=(tax+price)*0.02;
                System.out.println(tax);
                break;

            case "imported":
                double surcharge=0.0;
                tax=price*0.1;
                double temp=price*1.1;
                if(temp<100)
                  surcharge=5;
                else if(temp>100 && temp<200)
                  surcharge=10;
                else
                  surcharge=0.05*temp;
                  tax+=surcharge;
                  System.out.println(tax);
                break;
          }

          if(tax<0)
            System.out.println("Wrong input values");
          else{
            double final_price=price+tax;
            System.out.println("final_price is:"+final_price);                //Final price including tax
            System.out.println("tax is:"+tax);                                //Final Tax

          }
          System.out.println("Do you want to enter the details of any other item(Y/N):");     //scanning if you want to enter other details
          char ch=sc.next().charAt(0);
          cont=ch;
        }
        }catch(Exception e){                                                  //catch block beginning- Exceptional Handling
        System.out.println("Given input is invalid");        

    }
  }
}
